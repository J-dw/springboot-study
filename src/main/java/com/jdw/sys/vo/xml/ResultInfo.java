package com.jdw.sys.vo.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "User")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResultInfo {
    @XmlElement(name = "ResultCode")
    private String ResultCode;
    @XmlElement(name = "ResultContent")
    private String ResultContent;

    public static ResultInfo of(String resultCode, String resultContent) {
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setResultCode(resultCode);
        resultInfo.setResultContent(resultContent);
        return resultInfo;
    }
}
