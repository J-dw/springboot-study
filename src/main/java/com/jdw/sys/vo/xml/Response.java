package com.jdw.sys.vo.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {
    @XmlElement(name = "Body")
    private ResultInfo resultInfo;
    @XmlElement(name = "Data")
    private User user;

    public static Response of(ResultInfo resultInfo, User user) {
        Response response = new Response();
        response.setResultInfo(resultInfo);
        response.setUser(user);
        return response;
    }

}
