package com.jdw.sys.vo.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "User")
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    @XmlElement(name = "user_id")
    private String userId;
    /**
     * 用户名
     */
    @XmlElement(name = "user_name")
    private String userName;
    /**
     * 职称
     */
    @XmlElement(name = "duty_name")
    private String dutyName;
    /**
     * 工号
     */
    @XmlElement(name = "user_other_id")
    private String userOtherId;
    /**
     * 密码 仅做了md5处理
     */
    @XmlElement(name = "password")
    private String password;

    public static User of(String userId, String userName, String dutyName, String userOtherId, String password) {
        User user = new User();
        user.setUserId(userId);
        user.setUserName(userName);
        user.setDutyName(dutyName);
        user.setUserOtherId(userOtherId);
        user.setPassword(password);
        return user;
    }

}
