package com.jdw.sys.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdw.springboot.statemachine.Events;
import com.jdw.springboot.statemachine.SimpleStateMachineService;
import com.jdw.springboot.statemachine.States;
import com.jdw.sys.entity.Demo;
import com.jdw.sys.vo.xml.Request;
import com.jdw.sys.vo.xml.Response;
import com.jdw.sys.vo.xml.ResultInfo;
import com.jdw.sys.vo.xml.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import static com.jdw.springboot.statemachine.Events.E1;
import static com.jdw.springboot.statemachine.Events.E2;
import static com.jdw.springboot.statemachine.States.S1;

/**
 * @author ListJiang
 * @class 请求相关测试类
 * @since 2020/8/2510:15
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class TestController {
    private final ObjectMapper objectMapper;
    private final SimpleStateMachineService simpleStateMachineService;
    private final StateMachine<States, Events> stateMachine;

    @RequestMapping("/changState")
    Object changState() throws Exception {
        StateMachine<States, Events> state = simpleStateMachineService.getStateMachine(S1);
        Map<String, Object> headers1 = Map.of("data", true);
        Mono<Message<Events>> event2 = Mono.fromSupplier(() -> new GenericMessage<>(E2, headers1));
        Map<String, Object> headers2 = Map.of("data", false);
        Mono<Message<Events>> event1 = Mono.just(MessageBuilder.createMessage(E1, new MessageHeaders(headers2)));
        state.sendEvent(event1).subscribe();
        state.sendEvent(event2).subscribe();
        return "hello world，state：" + state.getState().getId();
    }


    @RequestMapping("/hello")
    Object hello() {
        return "hello world";
    }

    @GetMapping("/stream-sse")
    public Flux<ServerSentEvent<String>> streamEvents() {
        return Flux.interval(Duration.ofSeconds(1))
                .map(sequence -> ServerSentEvent.<String>builder()
                        .id(String.valueOf(sequence))
                        .event("periodic-event")
                        .data("SSE - " + LocalTime.now())
                        .build());
    }

    @RequestMapping("/json/out")
    public Object objectToJson() {
        Demo demo = new Demo();
        demo.setCreateTime(LocalDateTime.now());
        demo.setUpdateTime(LocalDateTime.now());
        return demo;
    }

    @RequestMapping("/json/in")
    public Object jsonToObject(@RequestBody JSONObject jsonObject) {
        Demo demo = jsonObject.toJavaObject(Demo.class);
        log.info(JSON.toJSONString(demo));
        return demo;
    }

    @RequestMapping("/test")
    public String jsonToObject() {
        return "http://gcjs.sczwfw.gov.cn/ele/?Form_Num=Z01";
    }

    @GetMapping("/test")
    public String jsonToObject1(@RequestBody JSONObject jsonObject) {
        return "http://gcjs.sczwfw.gov.cn/ele/?Form_Num=Z01";
    }

    /**
     * 请求参数如下所示
     * <?xml version="1.0" encoding="UTF-8"?>
     * <Request>
     * <Data>
     * <user_id>1188</user_id>
     * </Data>>
     * </Request>
     * 响应参数如下所示
     * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
     * <Response>
     * <Body>
     * <ResultCode>0</ResultCode>
     * </Body>
     * <Data>
     * <user_id>1188</user_id>
     * <user_name>周令明</user_name>
     * <duty_name>护师</duty_name>
     * <user_other_id>1188</user_other_id>
     * <password>b6fda2f22404f3487c007002ab90d94e</password>
     * </Data>
     * </Response>
     */
    @RequestMapping(value = "/csp/hsb/DHC.Published.PUB0007.BS.PUB0007.cls1", consumes = MediaType.TEXT_XML_VALUE, produces = MediaType.TEXT_XML_VALUE)
    public Object tt1(@RequestBody Request str) throws JsonProcessingException {
        log.info(objectMapper.writeValueAsString(str));
        return Response.of(ResultInfo.of("0", null),
                User.of(
                        "1188",
                        "周令明",
                        "护师",
                        "1188",
                        "b6fda2f22404f3487c007002ab90d94e"
                ));
    }

    /**
     * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dhcc="http://www.dhcc.com.cn">
     * <soapenv:Header/>
     * <soapenv:Body>
     * <dhcc:DhcService>
     * <!--Optional:-->
     * \s
     * <!--Optional:-->
     * <dhcc:input1>MES0029</dhcc:input1>
     * <dhcc:input2><![CDATA[]]>
     * </dhcc:input2>
     * </dhcc:DhcService>
     * </soapenv:Body>
     * </soapenv:Envelope>
     *
     * @param str
     * @return
     */
    @RequestMapping(value = "/csp/hsb/DHC.Published.PUB0007.BS.PUB0007.cls", consumes = MediaType.TEXT_XML_VALUE, produces = MediaType.TEXT_XML_VALUE)
    public Object tt2(@RequestBody String str, @Value("${spring.application.name}") String name) {
        log.info("request body:\n{}", str);
        String useId = str.substring(str.indexOf("<user_id>") + 9, str.indexOf("</user_id>")).trim();
        if ("1188".equals(useId)) {
            return Response.of(ResultInfo.of("0", null),
                    User.of(
                            "1188",
                            name,
                            "护师",
                            "1188",
                            "b6fda2f22404f3487c007002ab90d94e"
                    ));
        } else {
            return "";
        }
//        return "<Response><Body><ResultCode>0</ResultCode><ResultContent/></Body><Data><user_name>周令明</user_name><duty_name>护师</duty_name><user_other_id>1188</user_other_id><password>b6fda2f22404f3487c007002ab90d94e</password></Data></Response>";
    }
}