package com.jdw.springboot.websocket;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class GreetingController {
    private final SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message, Principal principal) {
        Greeting greeting = new Greeting("Hello, " + HtmlUtils.htmlEscape(message.name()) + "!");
        messagingTemplate.convertAndSendToUser(principal.getName(), "/greetings", greeting);
        return greeting;
    }

    @RequestMapping("/stomp")
    public String stomp() {
        return "stomp";
    }

    @RequestMapping("/sockJs")
    public String sockJs() {
        return "sockJs";
    }

}
