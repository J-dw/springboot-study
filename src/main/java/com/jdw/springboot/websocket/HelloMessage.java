package com.jdw.springboot.websocket;

public record HelloMessage(String name) {
}
