package com.jdw.springboot.config;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.Lifecycle;
import org.springframework.stereotype.Component;

/**
 * Spring Bean 生命周期钩子案例
 * <span>
 *     @PostConstruct >> InitializingBean.afterPropertiesSet >> @PreDestroy >> DisposableBean.destroy
 * </span>
 */
@Slf4j
@Component
public class SpringLifeCycleComponent implements InitializingBean, DisposableBean, Lifecycle {
    public SpringLifeCycleComponent() {
        log.info("invoke custom construct method");
    }

    @PostConstruct
    void postConstruct() {
        log.info("invoke postConstruct method");
    }

    @Override
    public void afterPropertiesSet() {
        log.info("invoke implement InitializingBean afterPropertiesSet method");
    }

    @PreDestroy
    void preDestroy() {
        log.info("invoke preDestroy method");
    }

    @Override
    public void destroy() {
        log.info("invoke destroy method");
    }

    @Override
    public void start() {
        log.info("invoke Lifecycle.start method");
    }

    @Override
    public void stop() {
        log.info("invoke Lifecycle.stop method");
    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
