package com.jdw.springboot.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.google.common.reflect.ClassPath;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTime序列化配置
 * 用于解决json转换时的格式问题
 *
 * @author ListJiang
 * @since 2022/01/03
 */
@Configuration
public class JacksonConfig {
    private static final String ENUM_PACKAGE = "com.jdw.springboot.enums";
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String TIME_PATTERN = "HH:mm:ss";

    /**
     * 统一配置 LocalDate、LocalDateTime、LocalTime 与 String 之间的互相转换
     * <p>
     * 最终效果:
     * {
     * "localDate": "2022-01-03",
     * "localDateTime": "2022-01-03 18:36:53",
     * "localTime": "18:36:53",
     * "date": "2022-01-03 18:36:53",
     * "calendar": "2022-01-03 18:36:53"
     * }
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        var module = new JavaTimeModule()
                .addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern(DATE_PATTERN)))
                .addDeserializer(LocalTime.class, new LocalTimeDeserializer(DateTimeFormatter.ofPattern(TIME_PATTERN)))
                .addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
        return builder -> builder
                .modules(module, customModule())
                .simpleDateFormat(DATE_TIME_PATTERN)
                .featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
                .serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(DATE_PATTERN)))
                .serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)))
                .serializers(new LocalTimeSerializer(DateTimeFormatter.ofPattern(TIME_PATTERN)))
                .serializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Bean
    ApplicationRunner customRunner(GenericConversionService conversionService) {
        return args -> {
            conversionService.addConverter(String.class, LocalDateTime.class, str -> LocalDateTime.parse(str, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)));
            conversionService.addConverter(String.class, LocalDate.class, str -> LocalDate.parse(str, DateTimeFormatter.ofPattern(DATE_PATTERN)));
            conversionService.addConverter(String.class, LocalTime.class, str -> LocalTime.parse(str, DateTimeFormatter.ofPattern(TIME_PATTERN)));
            // 支持 GET 传参直接接受枚举
            conversionService.addConverterFactory(new CustomStringToEnumConverterFactor());
        };
    }

    private Module customModule() {
        SimpleModule simpleModule = new SimpleModule();
        try {
            ClassPath classPath = ClassPath.from(this.getClass().getClassLoader());
            for (ClassPath.ClassInfo allClass : classPath.getTopLevelClassesRecursive(ENUM_PACKAGE)) {
                Class<?> load = allClass.load();
                simpleModule.addDeserializer(load, new CustomEnumDeserializer(load));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return simpleModule;
    }
}
