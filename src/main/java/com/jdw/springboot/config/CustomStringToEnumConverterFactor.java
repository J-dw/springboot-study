package com.jdw.springboot.config;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.lang.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 支持 GET 传参直接接受枚举
 */
@Slf4j
public class CustomStringToEnumConverterFactor implements ConverterFactory<String, Enum> {
    @Override
    public <T extends Enum> Converter<String, T> getConverter(Class<T> targetType) {
        return new StringToEnum(targetType);
    }

    private static class StringToEnum<T extends Enum> implements Converter<String, T> {
        private final ConcurrentHashMap<String, T> enumMap = new ConcurrentHashMap<>();

        StringToEnum(Class<T> enumeClass) {
            Method jsonMethod = null;
            Field jsonField = null;
            for (Field field : enumeClass.getDeclaredFields()) {
                for (Annotation annotation : field.getDeclaredAnnotations()) {
                    if (annotation instanceof JsonValue) {
                        jsonField = field;
                        break;
                    }
                }
            }
            for (Method declaredMethod : enumeClass.getDeclaredMethods()) {
                for (Annotation annotation : declaredMethod.getDeclaredAnnotations()) {
                    if (annotation instanceof JsonValue) {
                        jsonMethod = declaredMethod;
                        break;
                    }
                }
            }
            if (jsonField != null) {
                boolean accessible = jsonField.isAccessible();
                jsonField.setAccessible(true);
                for (Field field : enumeClass.getFields()) {
                    try {
                        Object enumVal = field.get(null);
                        if (enumeClass.isInstance(enumVal)) {
                            enumMap.put(Objects.toString(jsonField.get(enumVal), ""), (T) enumVal);
                        }
                    } catch (IllegalAccessException e) {
                        log.warn("自定义 JSON 反序列化获取属性出错", e);
                    }
                }
                jsonField.setAccessible(accessible);
            }
            if (jsonMethod != null) {
                boolean accessible = jsonMethod.isAccessible();
                jsonMethod.setAccessible(true);
                for (Field field : enumeClass.getFields()) {
                    try {
                        Object enumVal = field.get(null);
                        if (enumeClass.isInstance(enumVal)) {
                            enumMap.put(Objects.toString(jsonMethod.invoke(enumVal), ""), (T) enumVal);
                        }
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        log.warn("自定义 JSON 反序列化调用方法出错", e);
                    }
                }
                jsonMethod.setAccessible(accessible);
            }
        }

        @Override
        @Nullable
        public T convert(String source) {
            if (source.isEmpty()) {
                return null;
            }
            return enumMap.get(source);
        }
    }
}
