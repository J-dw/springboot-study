package com.jdw.springboot.config;

import io.micrometer.core.instrument.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.DefaultConnectionKeepAliveStrategy;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.util.TimeValue;
import org.apache.hc.core5.util.Timeout;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * restTemplate配置类
 *
 * @author ListJiang
 * @since 2021/2/18 15:42
 */
@Slf4j
@Configuration
public class RestTemplateConfig {


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .requestFactory(this::httpRequestFactory)
                .additionalInterceptors(loggingRequestInterceptor())
                .build();
    }

    public ClientHttpRequestFactory httpRequestFactory() {
        return new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient()));
    }

    public HttpClient httpClient() {
        // 长连接保持30秒
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        //设置整个连接池最大连接数 根据自己的场景决定
        connectionManager.setMaxTotal(500);
        //同路由的并发数,路由是对maxTotal的细分
        connectionManager.setDefaultMaxPerRoute(500);
        //requestConfig
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(10, TimeUnit.SECONDS)
                .setConnectionKeepAlive(TimeValue.ofSeconds(10))
                .setResponseTimeout(Timeout.of(10, TimeUnit.SECONDS))
                .build();
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                // 保持长连接配置，需要在头添加Keep-Alive
                .setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())
                .setRetryStrategy(new DefaultHttpRequestRetryStrategy(3, TimeValue.ofSeconds(3)))
                .build();
    }

    /**
     * 日志打印
     *
     * @return 自定义 http 请求拦截器
     */
    public ClientHttpRequestInterceptor loggingRequestInterceptor() {
        return (request, body, execution) -> {
            if (log.isDebugEnabled()) {
                log.debug("request_uri          : \t{}", request.getURI());
                log.debug("request_method       : \t{}", request.getMethod());
                log.debug("request_headers      : \t{}", request.getHeaders());
                log.debug("request_body         : \t{}", new String(body, StandardCharsets.UTF_8));
            }
            ClientHttpResponse response = execution.execute(request, body);
            if (log.isDebugEnabled()) {
                log.debug("response_status      : \t{}", response.getStatusCode());
                log.debug("response_headers     : \t{}", response.getHeaders());
                MediaType contentType = response.getHeaders().getContentType();
                if (MediaType.APPLICATION_JSON.isCompatibleWith(contentType)) {
                    log.debug("response_body        : \t{}", IOUtils.toString(response.getBody(), StandardCharsets.UTF_8));
                } else {
                    log.debug("response_content_type is not application/json");
                }
            }
            return response;
        };
    }
}
