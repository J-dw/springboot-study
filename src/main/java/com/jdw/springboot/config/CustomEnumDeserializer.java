package com.jdw.springboot.config;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 支持实体转换时，字符串类型属性，自动映射为枚举属性
 *
 * @param <T>
 */
@Slf4j
public class CustomEnumDeserializer<T extends Enum<T>> extends JsonDeserializer<T> {
    private final ConcurrentHashMap<String, T> enumMap = new ConcurrentHashMap<>();

    public CustomEnumDeserializer(Class<T> enumeClass) {
        if (enumeClass.getEnumConstants().length > 0 && enumeClass.isEnum()) {
            Method jsonMethod = null;
            Field jsonField = null;
            for (Field field : enumeClass.getDeclaredFields()) {
                for (Annotation annotation : field.getDeclaredAnnotations()) {
                    if (annotation instanceof JsonValue) {
                        jsonField = field;
                        break;
                    }
                }
            }
            for (Method declaredMethod : enumeClass.getDeclaredMethods()) {
                for (Annotation annotation : declaredMethod.getDeclaredAnnotations()) {
                    if (annotation instanceof JsonValue) {
                        jsonMethod = declaredMethod;
                        break;
                    }
                }
            }
            if (jsonField != null && enumeClass.getEnumConstants().length > 0) {
                boolean accessible = jsonField.canAccess(enumeClass.getEnumConstants()[0]);
                jsonField.setAccessible(true);
                for (T enumConstant : enumeClass.getEnumConstants()) {
                    try {
                        enumMap.put(jsonField.get(enumConstant).toString(), enumConstant);
                    } catch (IllegalAccessException e) {
                        log.warn("自定义 JSON 反序列化获取属性出错", e);
                    }
                }
                jsonField.setAccessible(accessible);
            }
            if (jsonMethod != null) {
                boolean accessible = jsonMethod.canAccess(enumeClass.getEnumConstants()[0]);
                jsonMethod.setAccessible(true);
                for (T enumConstant : enumeClass.getEnumConstants()) {
                    try {
                        enumMap.put(jsonMethod.invoke(enumConstant).toString(), enumConstant);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        log.warn("自定义 JSON 反序列化获取属性出错", e);
                    }
                }
                jsonMethod.setAccessible(accessible);
            }
        }
    }

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return enumMap.get(p.getText());
    }
}

