package com.jdw.springboot.statemachine;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

import java.util.EnumSet;

import static com.jdw.springboot.statemachine.Events.E1;
import static com.jdw.springboot.statemachine.Events.E2;
import static com.jdw.springboot.statemachine.States.*;

/**
 * 自定义状态机工厂配置
 * <br/>
 * <code>
 * <a href="https://docs.spring.io/spring-statemachine/docs/4.0.0/reference/index.html#devdocs">官方案例参考</a>
 * </code>
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SimpleStateMachineService {

    public StateMachine<States, Events> getStateMachine(States initStates) throws Exception {

        StateMachineBuilder.Builder<States, Events> builder = StateMachineBuilder.builder();
        builder.configureConfiguration()
                .withConfiguration().autoStartup(true)
                .and()
                .withVerifier().enabled(true);

        builder.configureStates().withStates()
                .initial(initStates)
                .states(EnumSet.allOf(States.class));

        builder.configureTransitions()
                .withExternal()
                .event(E1).source(S1).target(S2)
                .guard(guard())
                .action(action())
                .and()
                .withExternal()
                .event(E2).source(S2).target(S0)
                .guard(guard())
                .action(action());

        return builder.build();

    }

    private Guard<States, Events> guard() {
        return stateModel -> {
            Object data = stateModel.getMessageHeader("data");
            return Boolean.TRUE.equals(data);
        };
    }

    private Action<States, Events> action() {
        return context -> log.info("源状态：{},目标状态：{}，事件：{}", context.getSource().getId(), context.getTarget().getId(), context.getEvent());
    }
}
