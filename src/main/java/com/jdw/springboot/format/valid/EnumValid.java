package com.jdw.springboot.format.valid;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 这玩意其实没什么使用的必要，大多直接枚举接收 + 非null 校验即可
 * @author ListJiang
 * @since 2023/7/12
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {EnumValueValidator.class})
public @interface EnumValid {

    String message() default "必须为指定值";

    String[] strValues() default {};

    int[] intValues() default {};

    boolean[] boolValues() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    // 指定多个时使用
    @Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        EnumValid[] value();
    }
}
