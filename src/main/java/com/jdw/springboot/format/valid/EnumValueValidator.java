package com.jdw.springboot.format.valid;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;

/**
 * @author ListJiang
 * @since 2023/7/12
 */
public class EnumValueValidator implements ConstraintValidator<EnumValid, Object> {

    private String[] strValues;
    private int[] intValues;
    private boolean[] boolValues;

    @Override
    public void initialize(EnumValid constraintAnnotation) {
        strValues = constraintAnnotation.strValues();
        intValues = constraintAnnotation.intValues();
        boolValues = constraintAnnotation.boolValues();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return switch (value) {
            case String s -> Arrays.asList(strValues).contains(s);
            case Integer i -> Arrays.asList(intValues).contains(i);
            case Boolean b -> Arrays.asList(boolValues).contains(b);
            case null, default -> true;
        };
    }
}
