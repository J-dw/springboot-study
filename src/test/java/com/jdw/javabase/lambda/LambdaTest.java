package com.jdw.javabase.lambda;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

@Slf4j
class LambdaTest {
    private volatile int i = 0;

    public int iec() {
        i++;
        log.info("invoke inc(),i={}", i);
        return i;
    }

    @Test
    void inc() {
        i = 0;
        log.info("before invoke supplier.create,i={}", i);
        Supplier<Integer> supplier = this::iec;
        log.info("after invoke supplier.create,i={}", i);
        log.info("before invoke supplier.get(),i={}", i);
        int j = supplier.get();
        log.info("after invoke supplier.get(),i={}", i);
        Assertions.assertEquals(1, j);
    }
}
