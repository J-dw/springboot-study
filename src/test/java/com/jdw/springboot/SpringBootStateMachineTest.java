package com.jdw.springboot;

import com.jdw.SpringbootApplication;
import com.jdw.springboot.statemachine.Events;
import com.jdw.springboot.statemachine.SimpleStateMachineService;
import com.jdw.springboot.statemachine.States;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import reactor.core.publisher.Mono;

import static com.jdw.springboot.statemachine.Events.E1;
import static com.jdw.springboot.statemachine.Events.E2;
import static com.jdw.springboot.statemachine.States.S1;

@Slf4j
@SpringBootTest(classes = SpringbootApplication.class)
@AutoConfigureMockMvc
class SpringBootStateMachineTest {
    @Autowired
    SimpleStateMachineService simpleStateMachineService;

    @Test
    void test1() throws Exception {
        StateMachine<States, Events> stateMachine = simpleStateMachineService.getStateMachine(S1);
        Mono<Message<Events>> event2 = Mono.fromSupplier(() -> new GenericMessage<>(E2));
        Mono<Message<Events>> event1 = Mono.just(MessageBuilder.withPayload(E1).build());
        stateMachine.sendEvent(event1).subscribe();
        stateMachine.sendEvent(event2).subscribe();
        Assertions.assertEquals(S1, stateMachine.getState().getId());
    }
}