package com.jdw.springboot.annotation;

import java.lang.annotation.*;

@Target(ElementType.ANNOTATION_TYPE)
@Documented()
@Retention(value = RetentionPolicy.SOURCE)
public @interface Schedules {
    CustomSchedule[] value();
}