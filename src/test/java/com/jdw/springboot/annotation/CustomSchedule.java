package com.jdw.springboot.annotation;


public @interface CustomSchedule {
    String time() default "morning";
}
