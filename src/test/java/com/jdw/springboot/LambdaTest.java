package com.jdw.springboot;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.*;

/**
 * jdk8函数式接口测试
 *
 * @author ListJiang
 * @since 2020/12/16 19:24
 */
@Slf4j
class LambdaTest {

    /**
     * 断言
     */
    @Test
    void Predicate() {
        Predicate<String> isHz = t -> {
            log.info("判断参数为：" + t);
            return t.equals("Hz");
        };
        Assertions.assertTrue(isHz.test("Hz"));
    }

    /**
     * 生产者
     */
    @Test
    void SupplierTest() {
        Supplier<String> getString = () -> {
            log.info("开始生产随机数字");
            return Double.toString(Math.ceil(Math.random()));
        };
        log.info(getString.get());
    }

    /**
     * 消费者
     */
    @Test
    void ConsumerTest() {
        Consumer<String> isEnd = t -> {
            log.info("开始消费" + t);
            log.info("开始消费" + t);
        };
        isEnd.accept("答题");
    }

    /**
     * 入参、出参不同类型
     */
    @Test
    void FunctionTestCode() {
        Function<Integer, Boolean> function = (t) -> {
            log.info("入参:" + t);
            return t.equals("1");
        };
        log.info("入参:" + function.apply(1));
    }

    /**
     * 两个入参、一个出参。切数据类型相同
     */
    @Test
    void BinaryOperatorTest() {
        BinaryOperator<Integer> binaryOperator = (t, y) -> t * y;
        log.info(binaryOperator.apply(4, 6).toString());
    }

    /**
     * 两个参数的消费者
     */
    @Test
    void biConsumerTest() {
        BiConsumer consumer = (x, y) -> {
            log.info("参数一:" + x.toString());
            log.info("参数er:" + y.toString());
        };
        consumer.accept(1, 2);
    }

    /**
     * 一元运算，入参出参同种类型
     */
    @Test
    void UnaryOperatorTest() {
        UnaryOperator<String> unaryOperator = t -> "test";
        log.info(unaryOperator.apply("test"));
    }

    /**
     * 二元运算，入参出参同种类型
     */
    @Test
    void UnaryOperatorTest2() {
        BinaryOperator<String> binaryOperator = (t, c) -> {
            return t + c;
        };
    }
}