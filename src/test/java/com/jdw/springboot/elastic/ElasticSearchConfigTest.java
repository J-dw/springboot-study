package com.jdw.springboot.elastic;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchAllQuery;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.TransportUtils;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.util.Objects;

@Slf4j
public class ElasticSearchConfigTest {

    @Test
    void connect() throws IOException {
        String fingerprint = "d6ff1b50ede110ce922e7dd720dcbbce24c633b7283775816144974413341983";

        SSLContext sslContext = TransportUtils.sslContextFromCaFingerprint(fingerprint);

        BasicCredentialsProvider credsProv = new BasicCredentialsProvider();
        credsProv.setCredentials(
                AuthScope.ANY, new UsernamePasswordCredentials("elastic", "2McrI8pTYOe2U0*b9XOO")
        );

        RestClient restClient = RestClient
                .builder(new HttpHost("10.8.133.233", 9200, "https"))
                .setHttpClientConfigCallback(hc -> hc
                        .setSSLContext(sslContext)
                        .setDefaultCredentialsProvider(credsProv)
                )
                .build();

// Create the transport and the API client
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        ElasticsearchClient esClient = new ElasticsearchClient(transport);

        SearchResponse<JSONObject> search = esClient.search(s -> s
                        .index("search_join_2023")
                        .query(q -> q.matchAll(new MatchAllQuery.Builder().build())),
                JSONObject.class);

        for (Hit<JSONObject> hit : search.hits().hits()) {
            Objects.requireNonNull(hit.source());
            log.info(hit.source().toJSONString());
        }
    }
}
