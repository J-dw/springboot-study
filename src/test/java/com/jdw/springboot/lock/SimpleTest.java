package com.jdw.springboot.lock;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

/**
 * 线程安全问题简单测试
 * 简单业务逻辑测试，一个数值减，一个数值加。确保两数之和不会因为多线程操作而出错即为线程安全
 *
 * @author ListJiang
 * @since 2021/3/29 17:07
 */
@Slf4j
public class SimpleTest {

    private int A = 100;
    private int B = 0;

    private void sleep_100() {
        // 自定义当前线程休眠100毫秒，模拟业务数据处理带来的时间消耗
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 业务代码一：未进行任何处理
    Runnable runnable1 = () -> {

        {
            sleep_100();
            A--;
            sleep_100();
            B++;
            sleep_100();
            log.info("Current Thread Name：{}", Thread.currentThread().getName());
        }

    };

    @Test
    void Test1() {
        IntStream.range(1, 101)
                // 转为并行流，会使用基于jdk的默认基础线程池 ForkJoinPool.commonPool，默认该线程池线程数为当前cpu核数
                .parallel()
                .forEach(_ -> runnable1.run());
        log.info("A===>" + A);
        log.info("B===>" + B);
    }

    // 业务代码二：给业务处理代码块加上synchronized关键字，锁基于 SimpleTest.class 构建
    Runnable runnable2 = () -> {
        synchronized (SimpleTest.class) {
            sleep_100();
            A--;
            sleep_100();
            B++;
            sleep_100();
            log.info("Current Thread Name：{}", Thread.currentThread().getName());
        }

    };

    @Test
    void Test2() {
        IntStream.range(1, 100)
                // 转为并行流，会使用基于jdk的默认基础线程池 ForkJoinPool.commonPool，默认该线程池线程数为当前cpu核数
                .parallel()
                .forEach(_ -> runnable2.run());
        log.info("A===>" + A);
        log.info("B===>" + B);
    }

    Lock lock = new ReentrantLock();
    // 业务代码三： 简单使用重入锁
    Runnable runnable3 = () -> {
        try {
            lock.lock();
            sleep_100();
            A--;
            sleep_100();
            B++;
            sleep_100();
            log.info("Current Thread Name：{}", Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    };

    @Test
    void Test3() {
        IntStream.range(1, 101)
                // 转为并行流，会使用基于jdk的默认基础线程池 ForkJoinPool.commonPool，默认该线程池线程数为当前cpu核数
                .parallel()
                .forEach(_ -> runnable3.run());
        IntStream.range(1, 101).forEach(_ -> runnable3.run());
        log.info("A===>" + A);
        log.info("B===>" + B);
        Assertions.assertEquals(0, A);
        Assertions.assertEquals(100, B);
    }

    @Test
    void threadCreateTest() {
        class CustomRunnable implements Runnable {
            @Override
            public void run() {
                log.info("Current Thread Name：{}", Thread.currentThread().getName());
            }
        }
        class CustomThread extends Thread {
            @Override
            public void run() {
                log.info("Current Thread Name：{}", Thread.currentThread().getName());
            }
        }
        class CustomCallable implements Callable<Void> {
            @Override
            public Void call() {
                log.info("Current Thread Name：{}", Thread.currentThread().getName());
                return null;
            }
        }
        IntStream.range(1, 10).forEach(_ -> {
            new CustomRunnable().run();
            new CustomThread().start();
            new CustomCallable().call();
        });
    }

    @Test
    void sss() {
        dd dd = new dd();
        Assertions.assertEquals(2, dd.test1());
    }

    static class dd {
        volatile int i = 0;

        public int test1() {
            try {
                i = 2;
                return i;
            } finally {
                i = 3;
            }
        }
    }


}