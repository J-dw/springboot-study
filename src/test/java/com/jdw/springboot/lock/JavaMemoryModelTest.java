package com.jdw.springboot.lock;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

/**
 * java 内存模型相关案例
 */
@Slf4j
class JavaMemoryModelTest {
    static class SynchronizedExample {
        int a = 0;
        boolean flag = false;

        public synchronized void writer() {
            a = 1;
            flag = true;
        }

        public synchronized void reader() {
            if (flag) {
                int i = a * a;
                log.info("a*a={}", i);
            }
        }
    }

    @Test
    void happenBeforeTest() {
        IntStream.range(0,101)
                .forEach(_->{
                    SynchronizedExample synchronizedExample = new SynchronizedExample();
                    new Thread(synchronizedExample::writer).start();
                    new Thread(synchronizedExample::reader).start();
                });

    }
}
