package com.jdw.springboot.lock;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * volatile 可见性保证，原子性不保证案例
 */
@Slf4j
public class VolatileAtomicityDemo {
    public volatile static int inc = 0;

    public void increase() {
        inc++;
    }

    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(5);
        VolatileAtomicityDemo volatileAtomicityDemo = new VolatileAtomicityDemo();
        List<CompletableFuture<Void>> cfs = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cfs.add(CompletableFuture.runAsync(() -> {
                for (int j = 0; j < 500; j++) {
                    volatileAtomicityDemo.increase();
                }
            }, threadPool));
        }
        // 等待1.5秒，保证上面程序执行完成
        CompletableFuture.allOf(cfs.toArray(new CompletableFuture[0])).join();
        log.info("" + inc);

        IntStream.range(0, 6)
                .parallel()
                .forEach(_ -> {
                    for (int j = 0; j < 500; j++) {
                        volatileAtomicityDemo.increase();
                    }
                });
        log.info("" + inc);
        threadPool.shutdown();
    }
}
