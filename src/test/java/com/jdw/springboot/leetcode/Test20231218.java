package com.jdw.springboot.leetcode;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.*;

@Slf4j
public class Test20231218 {

    public List<List<Integer>> fourSum(int[] nums, int target) {
        Map<String, List<Integer>> resultMap = new HashMap<>();
        if (nums.length >= 4) {
            Arrays.sort(nums);
            for (int i = 0; i < nums.length; i++) {
                for (int j = nums.length - 1; j >= 0; j--) {
                    int left = i;
                    int right = j;
                    int leftIndex = left + 1;
                    int rightIndex = right - 1;
                    while (leftIndex < rightIndex) {
                        long tmp = 0L + nums[left] + nums[leftIndex] + nums[rightIndex] + nums[right];
                        if (tmp < target) {
                            leftIndex++;
                        } else if (tmp > target) {
                            rightIndex--;
                        } else {
                            resultMap.put("" + nums[left] + nums[leftIndex] + nums[rightIndex] + nums[right], Arrays.asList(nums[left], nums[leftIndex], nums[rightIndex], nums[right]));
                            leftIndex++;
                            rightIndex--;
                        }
                    }
                }
            }
        }
        return resultMap.values().stream().toList();
    }

    @Test
    void fourSumTest() {
        Assertions.assertEquals(fourSum(new int[]{1000000000, 1000000000, 1000000000, 1000000000}, -294967296), List.of());
        Assertions.assertEquals(fourSum(new int[]{-2, -1, 0, 0, 1, 2}, 0), List.of(List.of(-2, -1, 1, 2), List.of(-2, 0, 0, 2), List.of(-1, 0, 0, 1)));
        Assertions.assertEquals(fourSum(new int[]{-3, -1, 0, 2, 4, 5}, 2), List.of(List.of(-3, -1, 2, 4)));
    }

    public int findPeakElement(int[] nums) {
        int n = nums.length;
        if (n == 1) return 0;
        int l = 0, r = n - 1;
        while (l < r) {
            int mid = l + r >> 1;
            if (nums[mid] < nums[mid + 1]) l = mid + 1;
            else r = mid;
        }
        return l;
    }

    @Test
    void findPeakElementTest() {
        Assertions.assertTrue(Set.of(2).contains(findPeakElement(new int[]{1, 2, 3, 1})));
        Assertions.assertTrue(Set.of(1, 5).contains(findPeakElement(new int[]{1, 2, 1, 3, 5, 6, 4})));
    }
}
