package com.jdw.springboot.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class Test20231116 {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int tmp = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < nums.length; i++) {
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum == target) {
                    return sum;
                } else {
                    if (Math.abs(sum - target) <= Math.abs(tmp - target)) {
                        tmp = sum;
                    }
                }
                if (sum < target) {
                    while (left < nums.length - 1 && nums[left] == nums[left + 1]) {
                        left++;
                    }
                    left++;
                }
                if (sum >= target) {
                    while (right > 1 && nums[right - 1] == nums[right]) {
                        right--;
                    }
                    right--;
                }
            }
        }
        return tmp;
    }

    @Test
    void test() {
        Assertions.assertEquals(-15, threeSumClosest(new int[]{-1000,-5,-5,-5,-5,-5,-5,-1,-1,-1}, -14));
        Assertions.assertEquals(15, threeSumClosest(new int[]{2, 3, 8, 9, 10}, 16));
        Assertions.assertEquals(3, threeSumClosest(new int[]{0, 1, 2}, 1));
        Assertions.assertEquals(2, threeSumClosest(new int[]{-1, 2, 1, -4}, 1));
        Assertions.assertEquals(0, threeSumClosest(new int[]{0, 0, 0}, 1));
    }
}
