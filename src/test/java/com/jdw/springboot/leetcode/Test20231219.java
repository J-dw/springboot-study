package com.jdw.springboot.leetcode;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class Test20231219 {


    public int[] findPeakGrid_1901(int[][] mat) {
        int top = 0, bot = mat.length - 1;
        int mid, maxIndex = 0;
        while (top <= bot) {
            mid = top + bot >> 1;
            int max = -1;
            for (int i = 0; i < mat[mid].length; i++) {
                if (mat[mid][i] > max) {
                    max = mat[mid][i];
                    maxIndex = i;
                }
            }
            if (bot != mid && mat[mid][maxIndex] > mat[mid + 1][maxIndex]) {
                bot = mid;
                continue;
            }
            if (mid + 1 < mat.length && mat[mid][maxIndex] < mat[mid + 1][maxIndex]) {
                top = mid + 1;
                continue;
            }
            return new int[]{mid, maxIndex};
        }
        return new int[]{};
    }

    @Test
    void findPeakGridTest() {
        Assertions.assertArrayEquals(new int[]{1, 4}, findPeakGrid_1901(new int[][]{{10, 20, 40, 50, 60, 70}, {1, 4, 2, 3, 500, 80}}));
        Assertions.assertArrayEquals(new int[]{0, 0}, findPeakGrid_1901(new int[][]{{7, 2, 3, 1, 2}, {6, 5, 4, 2, 1}}));
        Assertions.assertArrayEquals(new int[]{0, 3}, findPeakGrid_1901(new int[][]{{41, 8, 2, 48, 18}, {16, 15, 9, 7, 44}, {48, 35, 6, 38, 28}, {3, 2, 14, 15, 33}, {39, 36, 13, 46, 42}}));
        Assertions.assertArrayEquals(new int[]{0, 1}, findPeakGrid_1901(new int[][]{{1, 4}, {3, 2}}));
        Assertions.assertArrayEquals(new int[]{1, 1}, findPeakGrid_1901(new int[][]{{10, 20, 15}, {21, 30, 14}, {7, 16, 32}}));
    }


    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ListNode listNode = (ListNode) o;
            return val == listNode.val && Objects.equals(next, listNode.next);
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }
    }

    public ListNode removeNthFromEnd_19(ListNode head, int n) {
        ListNode[] nodes = new ListNode[30];
        int i = 0;
        ListNode node = head;
        while (node != null) {
            nodes[i++] = node;
            node = node.next;
        }
        if (n == i) {
            return nodes[1];
        }
        if (n < i) {
            nodes[i - n - 1].next = nodes[i - n + 1];
        }
        return head;
    }

    @Test
    void removeNthFromEndTest() {
        ListNode listNode1 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        ListNode listNode2 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(5))));
        assertEquals(listNode2, removeNthFromEnd_19(listNode1, 2));
    }


    public boolean match(char a, char b) {
        return (a == '(' && b == ')')
                || (a == '{' && b == '}')
                || (a == '[' && b == ']');
    }

    public boolean isValid_20(String s) {
        if (s == null || s.isEmpty()) return false;
        Stack<Character> stack = new Stack<>();
        Stack<Character> tmp = new Stack<>();
        for (char c : s.toCharArray()) {
            stack.push(c);
        }
        while (!stack.isEmpty()) {
            if (tmp.isEmpty()) {
                tmp.push(stack.pop());
            } else {
                if (match(stack.peek(), tmp.peek())) {
                    stack.pop();
                    tmp.pop();
                } else {
                    tmp.push(stack.pop());
                }
            }
        }
        return tmp.isEmpty();
    }


    public boolean isValid1_20(String s) {
        Map<Character, Character> map = Map.of('{', '}', '(', ')', '[', ']');
        Stack<Character> stack = new Stack<>();
        stack.push('?');
        for (Character ch : s.toCharArray()) {
            if (map.containsKey(ch)) stack.push(ch);
            else {
                if (!ch.equals(map.get(stack.pop()))) return false;
            }
        }
        return stack.size() == 1;
    }

    @Test
    void isValid_20() {
        Assertions.assertTrue(isValid1_20("()"));
        Assertions.assertFalse(isValid1_20("(){}}{"));
        Assertions.assertFalse(isValid1_20("["));
        Assertions.assertTrue(isValid1_20("()[]{}"));
        Assertions.assertFalse(isValid1_20("(]"));
        Assertions.assertTrue(isValid_20("()"));
        Assertions.assertFalse(isValid_20("(){}}{"));
        Assertions.assertFalse(isValid_20("["));
        Assertions.assertTrue(isValid_20("()[]{}"));
        Assertions.assertFalse(isValid_20("(]"));
    }


    public ListNode mergeTwoLists_21(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        ListNode result = new ListNode();
        ListNode node = result;
        while (true) {
            if (list1 == null) {
                node.next = list2;
                break;
            }
            if (list2 == null) {
                node.next = list1;
                break;
            }
            if (list1.val < list2.val) {
                node.next = list1;
                node = node.next;
                list1 = list1.next;
            } else {
                node.next = list2;
                node = node.next;
                list2 = list2.next;
            }
        }
        return result.next;
    }

    @Test
    void mergeTwoListsTest() {

        ListNode listNode1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode listNode2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        ListNode listNode3 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(4))))));
        assertEquals(listNode3, (mergeTwoLists_21(listNode1, listNode2)));
        assertNull((mergeTwoLists_21(null, null)));
        assertEquals(new ListNode(0), (mergeTwoLists_21(null, new ListNode(0))));

    }

    public ListNode mergeKLists(ListNode[] lists) {
        ListNode result = new ListNode();
        ListNode node = result;
        PriorityQueue<ListNode> priorityQueue = new PriorityQueue<>(Comparator.comparingInt(a -> a.val));
        for (ListNode list : lists) {
            if (list != null) {
                priorityQueue.add(list);
            }
        }
        while (!priorityQueue.isEmpty()) {
            ListNode poll = priorityQueue.poll();
            node.next = poll;
            node = node.next;
            if (poll.next != null)
                priorityQueue.add(poll.next);
        }
        return result.next;
    }


    @Test
    void mergeKLists23Test() {
        ListNode listNode1 = new ListNode(1, new ListNode(4, new ListNode(5)));
        ListNode listNode2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        ListNode listNode3 = new ListNode(2, new ListNode(6));
        ListNode listNode4 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(4, new ListNode(5, new ListNode(6))))))));
        assertEquals(listNode4, mergeKLists(new ListNode[]{listNode1, listNode2, listNode3}));
    }

    public ListNode swapPairs(ListNode head) {
        ListNode result = new ListNode();
        ListNode nodeParent = result;
        ListNode node = head;
        result.next = node;
        while (node != null) {
            if (node.next != null) {
                // 设置新的引用
                ListNode next = node.next;
                ListNode nextParent = node;
                ListNode nexNode = next.next;

                nodeParent.next = next;
                next.next = node;
                node.next = null;

                // 刷新迭代引用
                node = nexNode;
                nodeParent = nextParent;
                nodeParent.next = node;
            } else {
                break;
            }
        }
        return result.next;
    }

    @Test
    void swapPairs24Test() {
        assertEquals(new ListNode(2, new ListNode(1, new ListNode(3))), swapPairs(new ListNode(1, new ListNode(2, new ListNode(3)))));
        assertEquals(new ListNode(1), swapPairs(new ListNode(1)));
        ListNode listNode1 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4))));
        ListNode listNode2 = new ListNode(2, new ListNode(1, new ListNode(4, new ListNode(3))));
        assertEquals(listNode2, swapPairs(listNode1));

    }
}
