package com.jdw.springboot.leetcode;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class Test20240409 {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        List<Stack<ListNode>> stacks = new ArrayList<>();
        int count = 0;
        ListNode cur = head;
        while (cur != null) {
            Stack<ListNode> stack = new Stack<>();
            stacks.add(stack);
            while (count++ < k && cur != null) {
                stack.push(cur);
                cur = cur.next;
            }
            count = 0;
        }
        ListNode res = new ListNode();
        cur = res;
        for (Stack<ListNode> stack : stacks) {
            if (stack.size() == k) {
                while (!stack.isEmpty()) {
                    cur.next = stack.pop();
                    cur = cur.next;
                }
                cur.next = null;
            } else {
                while (stack.size() != 1) {
                    stack.pop();
                }
                cur.next = stack.pop();
            }
        }
        return res.next;
    }

    @Test
    void reverseKGroupTest() {
        ListNode head;
        ListNode res;
        head = new ListNode(1, new ListNode(2));
        res = reverseKGroup(head, 2);
        while (res != null) {
            System.out.println(res.val);
            res = res.next;
        }
        head = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        res = reverseKGroup(head, 3);
        while (res != null) {
            System.out.println(res.val);
            res = res.next;
        }
        head = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        res = reverseKGroup(head, 2);
        while (res != null) {
            System.out.println(res.val);
            res = res.next;
        }
    }
}
