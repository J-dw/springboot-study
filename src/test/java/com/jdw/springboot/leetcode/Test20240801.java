package com.jdw.springboot.leetcode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class Test20240801 {
    public int maxmiumScore(int[] cards, int cnt) {
        Arrays.sort(cards);
        int result = 0;
        int mA = 0, mB = 0;
        for (int i = 0; i < cnt; i++) {
            int card = cards[cards.length - cnt + i];
            result += card;
            if (mA == 0 && (card & 1) != 0) {
                mA = card;
            }
            if (mB == 0 && (card & 1) == 0) {
                mB = card;
            }
        }
        int tempA = 0, tempB = 0;
        if ((result & 1) == 0) return result;
        for (int i = cards.length - cnt - 1; i >= 0; i--) {
            int card = cards[i];
            // 最小奇数不为0，换个最大的偶数
            if (mA != 0 && (card & 1) == 0) {
                tempA = result - mA + card;
                break;
            }
        }
        for (int i = cards.length - cnt - 1; i >= 0; i--) {
            int card = cards[i];
            // 最小偶数不为0，换个最大的奇数
            if (mB != 0 && (card & 1) != 0) {
                tempB = result - mB + card;
                break;
            }
        }
        int max = Math.max(tempA, tempB);
        if ((max & 1) == 0) return max;
        return 0;
    }


    @Test
    void reverseKGroupTest() {
        Assertions.assertEquals(18, maxmiumScore(new int[]{1, 2, 8, 9}, 3));
        Assertions.assertEquals(4, maxmiumScore(new int[]{9, 7, 1, 4, 9}, 1));
        Assertions.assertEquals(0, maxmiumScore(new int[]{3, 3, 1}, 1));
    }
}
