package com.jdw.springboot.leetcode;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

@Slf4j
class Test1 {
    static class OrderedStream {
        String[] strings;

        public OrderedStream(int n) {
            strings = new String[n];
        }

        public List<String> insert(int idKey, String value) {
            strings[idKey - 1] = value;
            List<String> result = new ArrayList<>(strings.length - idKey + 1);
            int i = idKey;
            boolean start = true;
            while (i-- > 0) {
                if (strings[i] == null) {
                    start = false;
                }
            }
            if (start) {
                int index = idKey - 1;
                while (index < strings.length && strings[index] != null) {
                    result.add(strings[index++]);
                }
            }
            return result;
        }
    }

    @Test
    void orderedStreamTest() {
        OrderedStream orderedStream = new OrderedStream(5);
        orderedStream.insert(3, "ccccc");
        orderedStream.insert(1, "aaaaa");
        orderedStream.insert(2, "bbbbb");
        orderedStream.insert(5, "eeeee");
        orderedStream.insert(4, "ddddd");
    }

    public int maxArea(int[] height) {
        int i = 0, j = height.length - 1;
        int mi = i, mj = j, maxArea = Math.min(height[mi], height[mj]) * (mj - mi);
        while (i < j) {
            if (height[i] < height[j]) {
                while (height[i] > height[i + 1] && i < height.length - 2) {
                    i++;
                }
                i++;
                mi = i;
            } else {
                while (height[j - 1] < height[j] && 0 < j) {
                    j--;
                }
                j--;
                mj = j;
            }
            maxArea = Math.max(Math.min(height[mi], height[mj]) * (mj - mi), maxArea);
        }
        return maxArea;
    }

    @Test
    void maxArea() {
        Assertions.assertEquals(24, maxArea(new int[]{1, 3, 2, 5, 25, 24, 5}));
        Assertions.assertEquals(49, maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
    }

    public String intToRoman(int num) {
        List<String> list = Arrays.asList("M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "III", "II", "I");
        Map<String, Integer> map = new HashMap<>();
        map.put("M", 1000);
        map.put("CM", 900);
        map.put("D", 500);
        map.put("CD", 400);
        map.put("C", 100);
        map.put("XC", 90);
        map.put("L", 50);
        map.put("XL", 40);
        map.put("X", 10);
        map.put("IX", 9);
        map.put("V", 5);
        map.put("IV", 4);
        map.put("III", 3);
        map.put("II", 2);
        map.put("I", 1);
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < num / map.get(list.get(i)); j++) {
                result += list.get(i);
            }
            num = num % map.get(list.get(i));
        }
        return result;
    }

    @Test
    void intToRoman() {
        Assertions.assertEquals("III", intToRoman(3));
        Assertions.assertEquals("IV", intToRoman(4));
        Assertions.assertEquals("IX", intToRoman(9));
        Assertions.assertEquals("LVIII", intToRoman(58));
        Assertions.assertEquals("MCMXCIV", intToRoman(1994));
    }

}

