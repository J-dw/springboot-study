package com.jdw.springboot.leetcode;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Slf4j
class Testabc {

    int countPoints(String rings) {
        Map<String, Set<String>> map = new HashMap<>();
        for (int i = 0; i < rings.length() / 2; i++) {
            String substring = rings.substring(i * 2, i * 2 + 2);
            String key = substring.substring(1, 2);
            String value = substring.substring(0, 1);
            map.computeIfAbsent(key, k -> new HashSet<>()).add(value);
        }
        return (int) map.values().stream()
                .filter(set -> set.size() == 3)
                .count();
    }

    @Test
    void test() {
        Assertions.assertEquals(1, countPoints("B0B6G0R6R0R6G9"));
        Assertions.assertEquals(1, countPoints("B0R0G0R9R0B0G0"));
        Assertions.assertEquals(0, countPoints("G4"));
    }

    public int dec(int i, int[] nums) {
        if (i == nums.length - 1) {
            return i;
        }
        if (nums[i] == nums[i + 1]) {
            return dec(i + 1, nums);
        } else {
            return i + 1;
        }
    }

    public int inc(int i, int[] nums) {
        if (i == 0) {
            return i;
        }
        if (nums[i] == nums[i - 1]) {
            return dec(i - 1, nums);
        } else {
            return i - 1;
        }
    }

    public List<List<Integer>> threeSum(int[] nums) {
        Set<String> code = new HashSet<>();
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] > 0) break;
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int L = i + 1, R = nums.length - 1;
            while (L < R) {
                int sum = nums[i] + nums[L] + nums[R];
                if (sum == 0) {
                    String s = nums[i] + "," + nums[L] + "," + nums[R];
                    if (!code.contains(s)) {
                        code.add(s);
                        result.add(Arrays.asList(nums[i], nums[L], nums[R]));
                    }
                    L++;
                    R--;
                } else if (sum > 0) {
                    R--;
                } else {
                    L++;
                }
            }
        }
        log.info(JSON.toJSONString(result));
        return result;
    }

    @Test
    void test12() {
        Assertions.assertEquals("[[-4,-2,6],[-4,0,4],[-4,1,3],[-4,2,2],[-2,-2,4],[-2,0,2]]", JSON.toJSONString(threeSum(new int[]{-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6})));
        Assertions.assertEquals("[[-1,0,1]]", JSON.toJSONString(threeSum(new int[]{-1, 0, 1, 0})));
        Assertions.assertEquals("[[-1,-1,2],[-1,0,1]]", JSON.toJSONString(threeSum(new int[]{-1, 0, 1, 2, -1, -4})));
        Assertions.assertEquals("[[0,0,0]]", JSON.toJSONString(threeSum(new int[]{0, 0, 0})));
        Assertions.assertEquals("[]", JSON.toJSONString(threeSum(new int[]{-2, -3, 0, 0, -2})));
    }


    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity("https://www.baidu.com", String.class);
        log.info(forEntity.getBody());
    }
}
