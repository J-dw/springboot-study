package com.jdw.springboot.leetcode;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class Test20231221 {


    public List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        tt(result, "", n, n);
        return result;
    }

    public void tt(List<String> result, String str, int left, int right) {
        if (left == 0 && right == 0) {
            result.add(str);
        }
        if (left <= right) {
            if (left > 0) {
                tt(result, str + "(", left - 1, right);
                tt(result, str + ")", left, right - 1);
            }
            if (left == 0) {
                tt(result, str + ")", left, right - 1);
            }
        }
    }


    @Test
    void generateParenthesis22Test() {
        List<String> strings = List.of("(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())", "(()())()", "(())(())", "(())()()", "()((()))", "()(()())", "()(())()", "()()(())", "()()()()");
        List<String> strings1 = List.of("()()()()", "(()())()", "(()(()))", "()()(())", "(((())))", "(())()()", "()((()))", "()(())()", "()(()())", "(()()())", "((()()))", "((()))()", "((())())");
        strings.stream()
                .filter(str -> !strings1.contains(str))
                .forEach(System.out::println);
        Assertions.assertIterableEquals(generateParenthesis(3), List.of("((()))", "(()())", "(())()", "()(())", "()()()"));
        Assertions.assertIterableEquals(generateParenthesis(1), List.of("()"));
    }
}
