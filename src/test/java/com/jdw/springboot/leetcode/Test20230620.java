package com.jdw.springboot.leetcode;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Test20230620 {
    public static double findMedianSortedArrays1(int[] nums1, int[] nums2) {
        // 先合并有序数组，时间复杂度 O(m + n)，空间复杂度 O(m + n)
        if (nums1.length == 0) {
            if ((nums2.length & 1) == 0) {
                return (nums2[nums2.length / 2 - 1] + nums2[nums2.length / 2]) / 2.0;
            } else {
                return nums2[nums2.length / 2];
            }
        }
        if (nums2.length == 0) {
            if ((nums1.length & 1) == 0) {
                return (nums1[nums1.length / 2 - 1] + nums1[nums1.length / 2]) / 2.0;
            } else {
                return nums1[nums1.length / 2];
            }
        }
        int arrayLength = 0;
        int index1 = 0, index2 = 0;
        int[] array = new int[nums1.length + nums2.length];
        wh:
        while (arrayLength != nums1.length + nums2.length) {
            while (index1 == nums1.length) {
                array[arrayLength++] = nums2[index2++];
                if (index2 == nums2.length) {
                    break wh;
                }
            }
            while (index2 == nums2.length) {
                array[arrayLength++] = nums1[index1++];
                if (index1 == nums1.length) {
                    break wh;
                }
            }

            if (nums1[index1] < nums2[index2]) {
                array[arrayLength++] = nums1[index1++];
            } else {
                array[arrayLength++] = nums2[index2++];
            }
        }
        if ((arrayLength & 1) == 1) {
            return array[arrayLength / 2];
        } else {
            return (array[arrayLength / 2 - 1] + array[arrayLength / 2]) / 2.0;
        }
    }

    public static double findMedianSortedArrays2(int[] nums1, int[] nums2) {
        // 从两头往中间找，偶数个取两个之和除以二。边界问题太复杂，不可取。
        return 0;
    }

    @Test
    void findMedianSortedArraysTest() {
        assertEquals(3, findMedianSortedArrays1(new int[]{1, 2}, new int[]{3, 4, 5}));
        assertEquals(2, findMedianSortedArrays1(new int[]{1, 3}, new int[]{2}));
        assertEquals(2.5, findMedianSortedArrays1(new int[]{1, 2}, new int[]{3, 4}));
    }
}
