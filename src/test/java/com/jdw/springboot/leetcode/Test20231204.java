package com.jdw.springboot.leetcode;

public class Test20231204 {
    public int strStr(String haystack, String needle) {
        if (haystack == null || needle == null || haystack.length() == 0 || needle.length() == 0) {
            return -1;
        }
        int length = needle.length();
        for (int i = 0; i < haystack.length() - length + 1; i++) {
            if (haystack.substring(i, i + length).equals(needle)) {
                return i;
            }
        }
        return -1;
    }
}
