package com.jdw.springboot.faker;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Locale;

@Slf4j
class BaseTest {
    @Test
    void name() {
        Faker faker = new Faker();
        final Name name = faker.name();
        log.info("firstName : " + name.firstName());
        log.info("username : " + name.username());
        log.info("bloodGroup : " + name.bloodGroup());
        log.info("suffix : " + name.suffix());
        log.info("title : " + name.title());
        log.info("lastName : " + name.lastName());
        log.info("nameWithMiddle : " + name.nameWithMiddle());
        log.info("fullName : " + name.fullName());
        log.info("name : " + name.name());
        log.info("prefix : " + name.prefix());
    }

    @Test
    void localName() {
        Faker faker = new Faker(Locale.CHINA);
        final Name name = faker.name();
        log.info("firstName : " + name.firstName());
        log.info("username : " + name.username());
        log.info("bloodGroup : " + name.bloodGroup());
        log.info("suffix : " + name.suffix());
        log.info("title : " + name.title());
        log.info("lastName : " + name.lastName());
        log.info("nameWithMiddle : " + name.nameWithMiddle());
        log.info("fullName : " + name.fullName());
        log.info("name : " + name.name());
        log.info("prefix : " + name.prefix());
    }
}
