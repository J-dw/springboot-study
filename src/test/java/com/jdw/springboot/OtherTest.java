package com.jdw.springboot;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * 其他与项目无关测试类
 *
 * @author ListJiang
 * @since 2020/8/188:16
 */
@Slf4j
class OtherTest {
    @Test
    void replaceAllTest() {
        String path = "/gcjs_lhch_wt/99d7e2c787c2abc3b164e6738d9e4c41";
        String regex = "/read/gcjs_lhch_wt/99d7e2c787c2abc3b164e6738d9e4c41";
        String replacement = "/jeecg-boot/api/gcjs/lhch/report/view/{mCbsnum}/{reportId}";
        String newPath = path.replaceAll(regex, replacement);
        log.info(newPath);
    }

    @Test
    void Base64() throws IOException, ClassNotFoundException {
        String msg = "士大夫撒qwertyio!@#$%^&*()_+1234567890-=/*-+|，。/、！@#￥%……&（（（）——+";
        byte[] encode = Base64.getEncoder().encode(msg.getBytes());
//        byte[] decode = Base64.getDecoder().decode(msg);
        log.info(Base64.getEncoder().encodeToString(msg.getBytes()));
        log.info(new String(encode, StandardCharsets.UTF_8));
        log.info(new String(Base64.getDecoder().decode(new String(encode, StandardCharsets.UTF_8))));
        //获取数据流Base64编码
        FileInputStream in = new FileInputStream("C:\\Users\\jdw\\Desktop\\新建文件夹\\timg (1).png");
        byte[] t = new byte[in.available()];
        in.read(t);
        String string = Base64.getEncoder().encodeToString(t);
        byte[] encode1 = Base64.getEncoder().encode(t);
        //Base64解码数据流
        byte[] decode = Base64.getDecoder().decode(string);
        FileOutputStream out = new FileOutputStream("C:\\Users\\jdw\\Desktop\\新建文件夹\\timg (s3).png");
        out.write(decode);
        in.close();
        out.flush();
        out.close();
        //获取String类型Base64编码
        String msg1 = "ds的手法对付《》？#￥%……&*（）";
        byte[] encode2 = Base64.getEncoder().encode(msg1.getBytes());
        //Base64解码为String类型
        String str1 = new String(Base64.getDecoder().decode(encode2));
        log.info("原文原文文" + msg1);
        log.info("编码解码后" + str1);
        //获取 Long 类型Base64编码
        Object oldObject = Long.valueOf("12354687");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(oldObject);
        //原对象字节数组
        byte[] bytes = outputStream.toByteArray();
        //编码后字节数组
        byte[] encode3 = Base64.getEncoder().encode(bytes);
        //解码后字节数组
        byte[] decode1 = Base64.getDecoder().decode(encode3);
        //此处可以通过循环判定内容是否变更，由于字节数组内容完全相等，所以”不想等“字符串永不打印
        if (bytes.length == decode1.length) {
            for (int i = 0; i < decode1.length; i++) {
                if (bytes[i] != decode1[i]) {
                    log.info("不相等");
                }
            }
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decode1);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        Object newObject = objectInputStream.readObject();
        log.info("新旧对象是否相等：" + newObject.equals(oldObject));
    }

    /**
     * 驼峰工具类测试
     */
    @Test
    void CaseFormat1() {
        assertEquals("testData", CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, "test-data"));
        assertEquals("testData", CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "test_data"));
        assertEquals("TestData", CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, "test_data"));
        assertEquals("testdata", CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "testdata"));
        assertEquals("test_data", CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "TestData"));
        assertEquals("test-data", CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, "testData"));
    }

    @Test
    void CaseFormat2() {
        List<String> list = new ArrayList<>();
        list.add("ID");
        list.add("PROJECTCODE");
        list.add("PROJECTNAME");
        list.add("INC_JGMC");
        list.add("INC_JYCS");
        list.add("INC_JGLXDH");
        list.add("JBR_MC");
        list.add("JBR_YDDH");
        list.add("ORDERNUM");
        list.add("AREAID");
        list.add("TYPECODE");
        list.add("PROJECTYEAR");
        list.add("CHANGETYPE");
        list.add("EXCHANGE");
        list.add("FILES");
        list.add("STATUS");
        list.add("PROJECTID");
        list.add("PROJECTVER");
        list.add("AREANAME");
        list.add("TYPENAME");
        list.add("APPLYTIME");
        list.add("SFYJXM");
        list.add("SFZDXM");
        list.add("XMSS");
        list.add("ZJLY");
        list.add("INC_JGDM");
        list.add("INC_ULN");
        list.add("INC_TOCL");
        list.add("INC_TUT");
        list.add("USERID");
        list.add("PROJECT_TYPE");
        list.add("PROJECT_NATURE");
        list.add("START_YEAR_MONTH");
        list.add("END_YEAR_MONTH");
        list.add("TOTAL_MONEY");
        list.add("PLACE_CODE");
        list.add("PLACE_CODE_DETAIL");
        list.add("INDUSTRY");
        list.add("SCALE_CONTENT");
        list.add("PROJECT_VALIFLAG");
        list.add("CREATED_TIME");
        list.add("UPDATED_TIME");
        list.add("LAND_AREA");
        list.add("BUILT_AREA");
        list.add("INDUSTRY_NAME");
        list.add("PLACE_CODE_NAME");
        list.add("ENGINERRING_CLASS");
        list.add("PRO_TYPE");
        list.add("PROJECT_FUND_ATTRIBUTE");
        list.add("ENGINERRING_CLASS_NAME");
        list.add("SBSJ");
        list.add("XMJSDDX");
        list.add("XMJSDDY");
        list.add("XMTZLY");
        list.add("TDHQFS");
        list.add("TDSFDSJFA");
        list.add("SFWCQYPG");
        list.add("XMSFWQBJ");
        list.add("DWTYSHXYDM");
        list.add("XMWQBJSJ");
        list.add("XMSJSJ");
        list.add("MAXSTAGE");
        list.add("BSSTATUS");
        list.add("XMSJAREAID");
        list.add("TYPEID");
        list.add("PARENTID");
        list.add("PROJECTNAMESON");
        list.add("PROJECTCODESON");
        list.add("APPLYZJLX");
        list.add("PROJECT_ATTRIBUTES");
        list.add("TARGETSTAGECODE");
        list.add("HISTORYSTAGECODES");
        list.add("PREPROJECTCODES");
        list.add("PROJECTSCOPE");
        list.add("inc_deputy");
        list.add("inc_deputy_mobile");
        list.add("PROJECT_SOURCE");
        list.add("BUILT_LENGTH");
        for (String str : list) {
            log.info(",p." + str + " as " + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str.toLowerCase()));
        }
    }

    @Test
    void IntegerTest() {
        Integer t = 0;
        log.info("" + t);
        t = -2147483648;
        log.info("" + t);
        t--;
        log.info("" + t);
    }

    @Test
    void test() {
        final String areaIds = "510703000000,510704000000,510705000000,510711000000,510713000000,510714000000,510715000000,510717000000,510722000000,510723000000,510725000000,510726000000,510727000000,510781000000";
        Assertions.assertTrue(areaIds.contains("510704000000"));

        for (int errNum = 0; errNum < 100; errNum++) {
            while (errNum < 20) {
                try {
                    log.info("" + 1 / (errNum % 2));
                    return;
                } catch (Exception exception) {
                    errNum++;
                    log.info("出错次数" + errNum);
                }

            }

        }
    }

    @Test
    void test1() {
        log.info("http://59.225.201.162:8087/group2/M00/73/13/rBKMWV_suVmATkUhAABVUhfkpuM416.pdf".replace("59.225.201.162", "172.18.140.105"));
    }

    @Test
    void test2() {
        int errNum = 0;
        while (errNum < 3) {
            try {
                log.info("调用一次");
                break;
            } catch (Exception exception) {
                errNum++;
            }
        }
    }

    @Test
    void test3() {
        String url = "http://172.18.0.103:31495/project/v1/task?sidx=deadline&sord=desc&page=1&rows=999999&cond=%7B%22filters%22:%7B%22groupOp%22:%22AND%22,%22groups%22:%5B%7B%22groupOp%22:%22OR%22,%22rules%22:%5B%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210636%22%7D%5D%7D,%7B%22groupOp%22:%22AND%22,%22rules%22:%5B%7B%22field%22:%22deadline%22,%22op%22:%22ge%22,%22data%22:1672502400000%7D,%7B%22field%22:%22startTime%22,%22op%22:%22le%22,%22data%22:1703952000000%7D,%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210636%22%7D%5D%7D%5D%7D%7D";
        String url2 = "http://172.18.0.103:31495/project/v1/task?sidx=deadline&sord=desc&page=1&rows=999999&cond=%7B%22filters%22:%7B%22groupOp%22:%22AND%22,%22groups%22:%5B%7B%22groupOp%22:%22OR%22,%22rules%22:%5B%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210657%22%7D%5D%7D,%7B%22groupOp%22:%22AND%22,%22rules%22:%5B%7B%22field%22:%22deadline%22,%22op%22:%22ge%22,%22data%22:1672502400000%7D,%7B%22field%22:%22startTime%22,%22op%22:%22le%22,%22data%22:1703952000000%7D,%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210657%22%7D%5D%7D%5D%7D%7D";
        String url3 = "http://172.18.0.103:31495/project/v1/task?sidx=deadline&sord=desc&page=1&rows=999999&cond=%7B%22filters%22:%7B%22groupOp%22:%22AND%22,%22groups%22:%5B%7B%22groupOp%22:%22OR%22,%22rules%22:%5B%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210332%22%7D%5D%7D,%7B%22groupOp%22:%22AND%22,%22rules%22:%5B%7B%22field%22:%22deadline%22,%22op%22:%22ge%22,%22data%22:1672502400000%7D,%7B%22field%22:%22startTime%22,%22op%22:%22le%22,%22data%22:1703952000000%7D,%7B%22field%22:%22assignedTo%22,%22op%22:%22eq%22,%22data%22:%2210332%22%7D%5D%7D%5D%7D%7D";
        String cookie2 = "C2AT=eyJraWQiOiJfeTBZX0E3dlRmR3VRd0xfSmJJLWJnIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYifQ.eyJhYyI6IjIwMjIwOTkiLCJzZXgiOiIwIiwiaXNzIjoiYzIiLCJvcmdpbnNpZHMiOlsiMTMtMTMtV2hwaFhGU2NSbEtiZzlDYmdCWng1QSIsIjEzLTEzLVJCbkZYa1dHVG9tNUp6a3FUbFVoaGciXSwib2lkIjpbIldocGhYRlNjUmxLYmc5Q2JnQlp4NUEiLCJSQm5GWGtXR1RvbTVKemtxVGxVaGhnIl0sInVpZCI6IjEwNjM2IiwiY2VyIjoiNDMwNTI0MTk5ODA0MTc0MDczIiwiY2VydHlwZSI6ImlkY2FyZCIsInBob25lIjoiMTM2OTE0OTIxOTciLCJuYW1lIjoi6JKL5b635paHIiwidW4iOiLokovlvrfmlociLCJ3bm8iOiIyMDIyMDk5IiwiZXhwIjoxNzA1MzcyMDY4LCJhaWQiOiI5bjhtY1ZmYlF6cUI2aXR3eS16Q1B3Iiwicm8iOlsiZGVmYXVsdCJdLCJpYXQiOjE3MDQ3NjcyNjgsImNpZCI6WyIxMy0xMyJdfQ.YDW2VK34cw48kQ9dFvbMTxfFRC_hei2Tvt_8cYsNZPUV1miWkcpAFgJHIv5be2qcCWOK44oU5OzmnPSWUQnHjZbO39SrVcjNVtKcqOIzlFcvsg160XBqCs0QPdUUQOMtEwl_j-aKdjEM8LE_V8N_toQHj7IKLWtA7nip_ygInOQ; C2RT=10636.dabca9ee99ade3a0ff7f28ce97a546c0";
        String cookie = "C2AT=eyJraWQiOiJTbDlsZG0wSlRuV0k4ZzZJT3FYM3R3IiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYifQ.eyJhYyI6IjIwMjIxMzAiLCJzZXgiOiIwIiwiaXNzIjoiYzIiLCJvcmdpbnNpZHMiOlsiMTMtMTMtV2hwaFhGU2NSbEtiZzlDYmdCWng1QSIsIjEzLTEzLVJCbkZYa1dHVG9tNUp6a3FUbFVoaGciXSwib2lkIjpbIldocGhYRlNjUmxLYmc5Q2JnQlp4NUEiLCJSQm5GWGtXR1RvbTVKemtxVGxVaGhnIl0sInVpZCI6IjEwNjU3IiwiY2VyIjoiNDMwMjA0MTk5NjAyMDExMDEyIiwiY2VydHlwZSI6ImlkY2FyZCIsInBob25lIjoiMTc2NzMyMzkyMjEiLCJuYW1lIjoi5YiY5a6X5b2mIiwidW4iOiLliJjlrpflvaYiLCJ3bm8iOiIyMDIyMTMwIiwiZXhwIjoxNzA0NzkwMTc2LCJhaWQiOiI5bjhtY1ZmYlF6cUI2aXR3eS16Q1B3Iiwicm8iOlsiZGVmYXVsdCJdLCJpYXQiOjE3MDQ3ODQxNzYsImNpZCI6WyIxMy0xMyJdfQ.B9YuQx-HlcFo8dlu7ytPZrZEQphBItH0fk-MtueiQ5-AT9cD9BZ8yStFOp4ETtPRmsFNlBoGBM5qse3KjifiwxhueudncBI2moKsuJaq9ahS5_WokJLU7FKJj11wt5KjZwHjcQS2etAr1FHEW3ED0NyhosqmuZd3LA-BqppfMRY; C2RT=10657.b73a193afd94aea647b0fc88eacbb428";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", cookie);
        headers.add("Referer", "http://172.18.0.103:31495/?v=20230919/");
        RequestEntity<String> stringHttpEntity = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
        ResponseEntity<JSONObject> exchange = restTemplate.exchange(stringHttpEntity, JSONObject.class);
        Map<String, AtomicInteger> map = new HashMap<>();
        Map<String, List<JSONObject>> map2 = new HashMap<>();
        JSONArray contents = exchange.getBody()
                .getJSONArray("contents");
        AtomicInteger total = new AtomicInteger(0);
        for (int i = 0; i < contents.size(); i++) {
            JSONObject content = contents.getJSONObject(i);
            if (!content.getString("name").contains("请假")) {
                String name = content.getJSONObject("bid").getString("name");
                AtomicInteger i1 = map.computeIfAbsent(name, key -> new AtomicInteger(0));
                int a = i1.get() + content.getIntValue("workload", 0);
                i1.set(a);
                List<JSONObject> maps = map2.computeIfAbsent(name, key -> new ArrayList<>());
                maps.add(content);
            }
        }
        map.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingInt(entity -> entity.getValue().get())))
                .forEach(
                        entity -> {
                            log.info(entity.getKey() + "   耗时  " + entity.getValue().get());
                            total.set(total.get() + entity.getValue().get());
                            List<JSONObject> workload = map2.get(entity.getKey())
                                    .stream()
                                    .sorted(Collections.reverseOrder(Comparator.comparingInt(t -> t.getIntValue("workload"))))
                                    .collect(Collectors.toList());
                            for (int i = 0; i < workload.size(); i++) {
                                JSONObject content = workload.get(i);
                                log.info(i + 1 + ". " + content.getString("name") + "  " + content.getIntValue("workload"));
                            }
                        }
                );
        log.info("总工时" + total);
    }
}