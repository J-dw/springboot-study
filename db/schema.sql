drop table if exists user;
create table user
(
    user_id     bigint                             not null comment '用户id' primary key,
    name        varchar(20)                        not null comment '姓名',
    age         int                                null comment '年龄',
    email       varchar(100)                       null comment '邮箱',
    sex         tinyint                            null comment '性别',
    status      tinyint                            null comment '有效状态',
    account     varchar(32)                        null comment '登录账号',
    password    varchar(64)                        null comment '登录密码',
    create_date datetime default (now())           null comment '创建时间',
    update_date datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间'
)
    comment '用户表';

drop table if exists role;
create table role
(
    role_id     bigint                             not null comment '角色id' primary key,
    role_name   varchar(20)                        not null comment '角色名',
    create_date datetime default (now())           null comment '创建时间',
    update_date datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间'
)
    comment '用户表';

drop table if exists user_role;
create table user_role
(
    ur_id   bigint not null comment '用户角色id' primary key,
    user_id bigint not null comment '用户id',
    role_id bigint not null comment '角色id'
)
    comment '用户角色表';

drop table if exists permission;
create table permission
(
    permission_id   bigint                             not null comment '权限id' primary key,
    permission_name varchar(20)                        not null comment '权限名',
    create_date     datetime default (now())           null comment '创建时间',
    update_date     datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '修改时间'
)
    comment '用户表';

drop table if exists role_permission;
create table role_permission
(
    rp_id         bigint not null comment '角色权限id' primary key,
    role_id       bigint not null comment '角色id',
    permission_id bigint not null comment '权限id'
)
    comment '角色权限表';

