# springboot-study

#### 介绍

ListJiang的学习之旅

#### 软件架构

软件架构说明

#### 安装教程

1. 确保jdk17+
2. 确保docker环境

```shell
docker run --name redis7 -p 6379:6379 -d redis:7.0.3 redis-server --save 60 1 --loglevel warning
 
```

#### 使用说明

1. 启动项目
> 通常来讲，是通用的项目启动流程，会自动创建 mysql， redis 环境进行项目启动。但是该方式基于 docker-compose 环境，compose.yml 文件修改后。
> 需要使用 `docker-compose down` 清理环境才行

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

